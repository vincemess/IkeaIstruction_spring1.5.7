package sample.Utility;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.pdf.*;
import sample.model.BinaryTrain;
import sample.model.Prototype;
import sample.model.SupportB;
import sample.model.SupportL;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by vincenzo on 30/06/17.
 */
public class PDFCreate {
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    private int page = 1;

    public  void createPDF(Prototype p, String nameFile,PDFUtility pdfUtility){


        String path = getPathFile(nameFile+".pdf","pdf");
        System.out.println(path);
        try {
            Document document = new Document(PageSize.A4,20,20,20,20);
            File FILE = new File(path);
            FILE.createNewFile();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(FILE));
            MyFooter event = new MyFooter();
            event.p = p;
            writer.setPageEvent(event);
            document.open();
            addMetaData(document,p);
            addTitlePage(document,p);
            addFirstPage(document,p);

            addContent(writer,document,pdfUtility);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addFirstPage(Document document, Prototype p) {

        ClassLoader classLoader = getClass().getClassLoader();
        String path = getPathFile("rex_logo.png","image_default");
        Image img = null;
        try {
            img = Image.getInstance(path);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadElementException e) {
            e.printStackTrace();
        }
        img.scaleAbsolute(300,150);
        img.setAlignment(Element.ALIGN_CENTER);

        Paragraph paraTitle = new Paragraph();
        Font titleFont = new Font(Font.FontFamily.HELVETICA, 30, Font.BOLD);
        Phrase title = new Phrase("ISTRUZIONI DI MONTAGGIO", titleFont);
        paraTitle.add(title);
        paraTitle.setAlignment(Element.ALIGN_CENTER);

        Paragraph paraInfo = new Paragraph();
        Font infoFont = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
        Phrase information = new Phrase("Rex, SwissCross, Mendrisio", infoFont);
        paraInfo.add(information);
        paraInfo.setAlignment(Element.ALIGN_CENTER);

        try {
            document.add( Chunk.NEWLINE );
            document.add(paraTitle);
            document.add( Chunk.NEWLINE );
            document.add(img);
            document.add( Chunk.NEWLINE );
            document.add(paraInfo);
        } catch (DocumentException e) {
            e.printStackTrace();
        }


        PdfPTable informationTable = new PdfPTable(2);
        informationTable.setHorizontalAlignment(Element.ALIGN_CENTER);
        informationTable.addCell(new PdfPCell(new Phrase("prototipo ")));
        informationTable.addCell(new PdfPCell(new Phrase(" "+p.getName())));

        informationTable.addCell(new PdfPCell(new Phrase("n. prototipo")));
        informationTable.addCell(new PdfPCell(new Phrase(" "+p.getNrArticle())));

        informationTable.addCell(new PdfPCell(new Phrase("n. model ")));
        informationTable.addCell(new PdfPCell(new Phrase(" "+p.getNrModel())));

        informationTable.addCell(new PdfPCell(new Phrase("nome cliente ")));
        informationTable.addCell(new PdfPCell(new Phrase(" "+p.getClient())));

        informationTable.addCell(new PdfPCell(new Phrase("prototipo id ")));
        informationTable.addCell(new PdfPCell(new Phrase(" "+p.getId())));

        try {
            document.add( Chunk.NEWLINE );
            document.add(informationTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        // inventary for binary
        PdfPTable mainTable = new PdfPTable(2);

        for (BinaryTrain binaryTrain : p.getBinaries()) {
            PdfPTable table = new PdfPTable(2);
            PdfPCell c = new PdfPCell(new Phrase("Binario"));
            c.setHorizontalAlignment(Element.ALIGN_CENTER);
            c.setVerticalAlignment(Element.ALIGN_CENTER);
            c.setBorderColor(BaseColor.BLACK);
            table.addCell(c);
            c = new PdfPCell(new Phrase(binaryTrain.getName()));
            table.addCell(c);
            c = new PdfPCell(new Phrase("supporto A"));
            table.addCell(c);
            c = new PdfPCell(new Phrase(" "+binaryTrain.getInventaryBinary().getNumberSupportA()));
            table.addCell(c);
            c = new PdfPCell(new Phrase("supporto B"));
            table.addCell(c);
            c = new PdfPCell(new Phrase(" "+binaryTrain.getInventaryBinary().getNumberSupportB()));
            table.addCell(c);
            c = new PdfPCell(new Phrase("supporto L"));
            table.addCell(c);
            c = new PdfPCell(new Phrase(" "+binaryTrain.getInventaryBinary().getNumberSupportL()));
            table.addCell(c);
            c = new PdfPCell(new Phrase("bulloni"));
            table.addCell(c);
            c = new PdfPCell(new Phrase(" "+(binaryTrain.getInventaryBinary().getNumberSupportL()* SupportL.NECESSARYSCREWS +
                    binaryTrain.getInventaryBinary().getNumberSupportL()* SupportB.NECESSARYSCREWS)));
            table.addCell(c);
            c = new PdfPCell(new Phrase("dado"));
            table.addCell(c);
            c = new PdfPCell(new Phrase(" "+(binaryTrain.getInventaryBinary().getNumberSupportL()* SupportL.NECESSARYSCREWS +
                    binaryTrain.getInventaryBinary().getNumberSupportL()* SupportB.NECESSARYSCREWS)));
            table.addCell(c);
            c = new PdfPCell(new Phrase("vite"));
            table.addCell(c);
            c = new PdfPCell(new Phrase(" "+(binaryTrain.getInventaryBinary().getNumberSupportL()* SupportL.NECESSARYSCREWS +
                    binaryTrain.getInventaryBinary().getNumberSupportL()* SupportB.NECESSARYSCREWS)));
            table.addCell(c);


            PdfPCell mainCell = new PdfPCell(table);
            mainCell.setPaddingTop(3.0f);
            mainCell.setPaddingBottom(3.0f);
            mainCell.setPaddingRight(3.0f);
            mainCell.setBorderColor(BaseColor.WHITE);
            mainTable.addCell(mainCell);

        }
        PdfPCell oddCell = new PdfPCell( new Phrase(" "));
        oddCell.setBorderColor(BaseColor.WHITE);
        mainTable.addCell(oddCell);
        try {
            document.add( Chunk.NEWLINE );
            document.add(mainTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.newPage();


    }

    private void addContent(PdfWriter writer,Document document,PDFUtility pdfUtility) throws DocumentException {

        Font font = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);

        for (int i = 0; i < pdfUtility.getPages().size(); i+=2) {
            document.add(Chunk.NEWLINE);
            Phrase p = new Phrase("istruzioni per il binario: "+pdfUtility.getPages().get(i).getInventaryBinary().getNameBinary(), font);
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
                    p, document.getPageSize().getWidth() /2 +document.leftMargin() ,
                    document.top() -15, 0);


            ArrayList<PagePDF> pages = new ArrayList<>();
            pages.add(pdfUtility.getPages().get(i));
            if(i+1 < pdfUtility.getPages().size())
                pages.add(pdfUtility.getPages().get(i+1));

            //addImage(document,pagePDF);
            createTable(document,pages);

            document.newPage();
        }
    }

    private void addTitlePage(Document document,Prototype p ) throws DocumentException {
            Paragraph preface = new Paragraph();
            // We add one empty line
            addEmptyLine(preface, 1);
            // Lets write a big header
            preface.add(new Paragraph("instruction for "+p.getName(), catFont));

            addEmptyLine(preface, 1);
            // Will create: Report generated by: _name, _date
            preface.add(new Paragraph(
                    "Pdf instruction generated by: " + System.getProperty("user.name") + ", " + LocalDate.now(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    smallBold));
            addEmptyLine(preface, 1);
    }
    private void createTable(Document document,ArrayList<PagePDF> pagePDFList)
            throws BadElementException {
        PdfPTable table = new PdfPTable(2);
            // order photo

        for (PagePDF pagePDF : pagePDFList) {
            int i =0;
            for (String s : pagePDF.getPaths()) {

                PdfPCell c = new PdfPCell();
                c.setHorizontalAlignment(Element.ALIGN_CENTER);
                c.setVerticalAlignment(Element.ALIGN_CENTER);
                c.setBorderColor(BaseColor.WHITE);
                if (i == 0)
                    s = addwriteonImage(s, pagePDF.getInventaryBinary().getNameBinary(),pagePDF.getNumberOfPiece(),pagePDF);
                else if(i==1 ){
                    s = addwriteonSecondImage(s, pagePDF.getInventaryBinary().getNameBinary(),pagePDF.getNumberOfPiece(),pagePDF);
                }

                Image img = null;
                try {
                    img = Image.getInstance(s);
                    img.scaleAbsolute(175, 175);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                c.setVerticalAlignment(Element.ALIGN_CENTER);
                c.setHorizontalAlignment(Element.ALIGN_CENTER);
                c.addElement(img);
                table.addCell(c);
                i++;
            }

            //PdfPCell c = new PdfPCell(new Phrase(pagePDF.getText()));
            //c.setHorizontalAlignment(Element.ALIGN_CENTER);
            //c.setVerticalAlignment(Element.ALIGN_CENTER);
            //c.setBorderColor(BaseColor.WHITE);
            //table.addCell(c);
        }

        try {
            document.add(table);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private String addwriteonSecondImage(String s, String nameBinary, int numberPiece, PagePDF pagePDF) {
        BufferedImage imageb = null;
        try {
            imageb = ImageIO.read(new File(s));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(pagePDF.getType().equals("supportB"))
            imageb = processScrew(imageb, SupportB.NECESSARYSCREWS*numberPiece);
        else if(pagePDF.getType().equals("supportL"))
            imageb = processScrew(imageb, SupportL.NECESSARYSCREWS*numberPiece);
        else if (pagePDF.getType().equals("grids"))
            imageb = processGrid(imageb, pagePDF.getInventaryBinary().getNumberSupportL());

        String pathFinale = "";
        if(!s.contains(nameBinary)){
            String[] parts = s.split("/");
            parts[parts.length-1] = nameBinary.concat("_").concat(parts[parts.length-1]);
            pathFinale = getPathFile(parts[parts.length-1],"image");
        }else
            pathFinale = s;


        try {
            ImageIO.write(imageb, "jpg", new File(pathFinale));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pathFinale;
    }


    private String addwriteonImage(String s, String pathFinale , int number, PagePDF pagePDF) {
        BufferedImage imageb = null;
        try {
            imageb = ImageIO.read(new File(s));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(pagePDF.getType().equals("grids")){
            imageb = processGridFirst(imageb, number,pagePDF.getInventaryBinary().getGridsNumber()[1]);
        }else{
            imageb = process(imageb, number);
        }
        String[] parts = s.split("/");
        parts[parts.length-1] = pathFinale.concat("_").concat(parts[parts.length-1]);

        pathFinale = getPathFile(parts[parts.length-1],"image");

        try {
            ImageIO.write(imageb, "jpg", new File(pathFinale));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pathFinale;

    }
    private static BufferedImage processGridFirst(BufferedImage old, int number,int numberCenterGrid ) {
        Graphics graphics = old.getGraphics();
        graphics.setColor(Color.BLACK);
        graphics.setFont(new java.awt.Font("Arial Black", Font.BOLD, 64));
        graphics.drawString("center grid number: "+numberCenterGrid, 180, 260);

        return old;
    }
    private static BufferedImage processGrid(BufferedImage old, int number) {
        Graphics graphics = old.getGraphics();
        graphics.setColor(Color.BLACK);
        graphics.setFont(new java.awt.Font("Arial Black", Font.BOLD, 32));
        graphics.drawString("x"+number , old.getWidth()/4+20, old.getHeight()/4);
        graphics.drawString("x"+number , old.getWidth()/4*2+60, old.getHeight()/4*2+20);
        graphics.drawString("x"+number , old.getWidth()/4*3+20, old.getHeight()/4*3+20);
        graphics.drawString("x"+number , old.getWidth()/4*2+120, old.getHeight()/4*2);

        return old;
    }
    private static BufferedImage processScrew(BufferedImage old, int number) {
        Graphics graphics = old.getGraphics();
        graphics.setColor(Color.BLACK);
        graphics.setFont(new java.awt.Font("Arial Black", Font.BOLD, 32));
        graphics.drawString("x"+number , old.getWidth()/4+20, old.getHeight()/4);
        graphics.drawString("x"+number , old.getWidth()/4*2+60, old.getHeight()/4*2+20);
        graphics.drawString("x"+number , old.getWidth()/4*3+20, old.getHeight()/4*3+20);

        return old;
    }

    private BufferedImage process(BufferedImage old, int number) {
        Graphics graphics = old.getGraphics();
        graphics.setColor(Color.BLACK);
        graphics.setFont(new java.awt.Font("Arial Black", Font.BOLD, 100));
        graphics.drawString(number +"x", 225, 190);

        return old;
    }
    private String getPathFile(String nomeFile, String folderFinal){
        String currentUsersHomeDir = System.getProperty("user.home");
        String folderProjectPath = currentUsersHomeDir + File.separator + "PROJECTREX";
        File dirProject = new File(folderProjectPath);
        if(!dirProject.exists()){
            dirProject.mkdir();
        }
        String folderResourcePath = folderProjectPath+File.separator+"resources";
        File dirResources = new File(folderResourcePath);
        if(!dirResources.exists()){
            dirResources.mkdir();
        }
        String finalfolderResourcePath = folderResourcePath+File.separator+folderFinal;
        File dirFolderFinal = new File(finalfolderResourcePath);
        if(!dirFolderFinal.exists()){
            dirFolderFinal.mkdir();
        }
        String filePath = finalfolderResourcePath + File.separator + nomeFile;
        return filePath;
    }


    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static void addMetaData(Document document,Prototype p) {
        document.addTitle("Prototipo "+ p.getName());
        document.addAuthor("Rex, SwissCross");
        document.addCreator("Rex, SwissCross");
    }
    class MyFooter extends PdfPageEventHelper {
        Font ffont = new Font(Font.FontFamily.HELVETICA, 9, Font.ITALIC);
        Font hfont = new Font(Font.FontFamily.HELVETICA, 11, Font.ITALIC);
        Prototype p;

        public void onEndPage(PdfWriter writer, Document document) {
            if(page != 1 ) {
                PdfContentByte cb = writer.getDirectContent();
                String h = "prototipo n: " + p.getNrArticle() + ", nome: "
                        + p.getName() + ", n. prototipo: " +p.getNrArticle()+ ", n. model: " +p.getNrModel()+ ", id prototipo: "
                        +p.getId() + ", cliente: " +p.getClient();
                Phrase header = new Phrase(h, hfont);
                Phrase footer = new Phrase("Rex, SwissCross, Mendrisio", ffont);
                Phrase numberPage = new Phrase(" "+page, ffont);


                Path currentRelativePath = Paths.get("");
                String s = currentRelativePath.toAbsolutePath().toString();
                String path = getPathFile("rex_logo.png", "image_default");
                Image img = null;
                try {
                    img = Image.getInstance(path);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BadElementException e) {
                    e.printStackTrace();
                }
                img.scaleAbsolute(100, 50);
                img.setAbsolutePosition(document.getPageSize().getWidth() * (2 / 3) + 10, document.bottom() - 10);
                try {
                    writer.getDirectContent().addImage(img);
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                ColumnText.showTextAligned(cb, Element.ALIGN_LEFT,
                        header,
                        document.leftMargin(),
                        document.top() + 4, 0);
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                        footer,
                        (document.getPageSize().getWidth()) / 2 + document.leftMargin(),
                        document.bottom() - 10, 0);
                ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT,
                        numberPage,
                        (document.getPageSize().getWidth())  - document.leftMargin(),
                        document.bottom() - 10, 0);
                page++;
            }else{
                page++;
            }


            //code skeleton to write page header

        }
    }


}
