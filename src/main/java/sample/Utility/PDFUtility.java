package sample.Utility;

import sample.model.InventaryBinary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 04/07/17.
 */
public class PDFUtility {
    List<PagePDF>  pages = new ArrayList<>();



    public List<PagePDF> getPages() {
        return pages;
    }

    public void setPages(List<PagePDF> pages) {
        this.pages = pages;
    }
}
