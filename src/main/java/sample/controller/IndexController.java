package sample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by vincenzo on 31/05/17.
 */
@Controller
public class IndexController {
    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String greetingRoot(Model model) {
        return "dashboard";
    }
}
