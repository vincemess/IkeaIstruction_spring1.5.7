package sample.controller;


import org.apache.commons.compress.utils.IOUtils;
import org.kabeja.dxf.*;
import org.kabeja.parser.DXFParser;
import org.kabeja.parser.ParseException;
import org.kabeja.parser.Parser;
import org.kabeja.parser.ParserBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sample.Utility.PDFCreate;
import sample.Utility.PDFUtility;
import sample.Utility.PagePDF;
import sample.Utility.ResponseAjax;
import sample.model.*;

import org.springframework.web.multipart.MultipartFile;
import sample.repo.*;
import sun.misc.BASE64Decoder;


import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by vincenzo on 31/05/17.
 */
@Controller
public class ModelController {

    @Autowired
    protected BinaryTrainRepository binaryRepository;
    @Autowired
    protected GridPolylineRepository gridPolylineRepository;
    @Autowired
    protected LineRepository lineRepository;
    @Autowired
    protected PointRepository pointRepository;
    @Autowired
    protected PrototypeRepository prototypeRepository;
    @Autowired
    protected RailRepository railRepository;
    @Autowired
    protected RailwaySleeperRepository railwaySleeperRepository;
    @Autowired
    protected SupportARepository supportARepository;
    @Autowired
    protected InventaryBinaryRepository inventaryBinaryRepository;
    @Autowired
    protected SupportBRepository supportBRepository;
    @Autowired
    protected SupportLRepository supportLRepository;


    //controller per aggiungere un nuovo prototipo
    @RequestMapping(value = "/prototype/add", method = RequestMethod.GET)
    public String greetingRoot(Model model) {

        //prototipo nuovo che verrà compilato
        model.addAttribute("newPrototype", new Prototype("name"));
        return "Prototype/addPrototype";
    }

    @RequestMapping(value = "/prototype/add", method = RequestMethod.POST)
    public ModelAndView addPrototype(RedirectAttributes redir, Model model, @ModelAttribute("newPrototype") Prototype prototype, @RequestParam("file") MultipartFile file, @RequestParam("layerG") String layerG, @RequestParam("layerR") String layerR) {
        ModelAndView modelAndView = new ModelAndView();
        String path = uploadFile(prototype.getName()+".dxf", file, "fileDXF");
        Parser parser = ParserBuilder.createDefaultParser();
        try {
            parser.parse(path, DXFParser.DEFAULT_ENCODING);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DXFDocument doc = parser.getDocument();

        // grids
        //trovo griglie nel file e creo gridpolyline
        List<DXFPolyline> lstGrids = doc.getDXFLayer(layerG).getDXFEntities(DXFConstants.ENTITY_TYPE_LWPOLYLINE);
        List<GridPolyline> gridPolylines = new ArrayList<>();
        for (int index = 0; index < lstGrids.size(); index++) {
            List<Point> points = new ArrayList<>();
            Bounds bounds = lstGrids.get(index).getBounds();

            Iterator<DXFVertex> vertexIterator = lstGrids.get(index).getVertexIterator();
            while (vertexIterator.hasNext()) {
                DXFVertex vertex = vertexIterator.next();

                points.add(new Point(vertex.getX(), vertex.getY()));
            }
            //elimino il quinto  vertice uguale al primo
            if(points.size() == 6){ // risolevere errore nel file 3 binari
                points = points.subList(0, points.size() - 1);
            }
            points = points.subList(0, points.size() - 1);
            gridPolylines.add(new GridPolyline("grid", points, calculateCenter(bounds), new Point(bounds.getMaximumX(), bounds.getMaximumY()), new Point(bounds.getMinimumX(), bounds.getMinimumY())));
        }
        //ordino griglie per posizione
        Collections.sort(gridPolylines, new Comparator<GridPolyline>() {
            public int compare(GridPolyline g1, GridPolyline g2) {
                if (g1.getCenter().getX() == g2.getCenter().getX())
                    return 0;
                else if (g1.getCenter().getX() >= g2.getCenter().getX())
                    return 1;
                else
                    return -1;
            }
        });
        int index1;
        //setto number nelle griglie
        for (int i = 1; i <= gridPolylines.size(); i++) {
            index1 = i - 1;
            gridPolylines.get(index1).setNumber(i);
        }


        //rails
        List<DXFLine> lstR = doc.getDXFLayer(layerR).getDXFEntities(DXFConstants.ENTITY_TYPE_LINE);
        List<Line> linesR = new ArrayList<>();
        for (int index = 0; index < lstR.size(); index++) {
            Bounds bounds = lstR.get(index).getBounds();
            Point center = new Point();
            center.setX(bounds.getMinimumX() + (bounds.getMaximumX() - bounds.getMinimumX()) / 2);
            center.setY(bounds.getMinimumY() + (bounds.getMaximumY() - bounds.getMinimumY()) / 2);
            Line line = new Line(new Point(lstR.get(index).getStartPoint().getX(), lstR.get(index).getStartPoint().getY()), new Point(lstR.get(index).getEndPoint().getX(), lstR.get(index).getEndPoint().getY()), lstR.get(index).getLength());
            line.setCenter(center);
            linesR.add(line);
        }

        //ordino rotaie
        List<Line> railsOrdered = orderRails(linesR);

        // ordino griglie e rotaie calcolo le translation dalla griglia centrale
        List<BinaryTrain> binaries = createBinary(railsOrdered, gridPolylines);
        prototype.setBinaries(binaries);
        //salve
        prototype = savePrototype(prototype);

        modelAndView.setViewName("redirect:/prototype/create/"+prototype.getId());
        return modelAndView;
    }
        //metodo per il salvataggio del prototipo
    private Prototype savePrototype(Prototype prototype) {
        for (BinaryTrain binary : prototype.getBinaries()) {
            pointRepository.save(binary.getCenter());
            pointRepository.save(binary.getTranslationFromOtherBinary());

            //grid save
            for (GridPolyline gridPolyline : binary.getGrids()) {
                pointRepository.save(gridPolyline.getCenter());
                pointRepository.save(gridPolyline.getMax());
                pointRepository.save(gridPolyline.getMin());
                pointRepository.save(gridPolyline.getObjectCoordinates());
                pointRepository.save(gridPolyline.getPoints());
                pointRepository.save(gridPolyline.getTranslationGridFromCenter());
            }
            gridPolylineRepository.save(binary.getGrids());

            // rail save
            pointRepository.save(binary.getRail().getCenter());
            pointRepository.save(binary.getRail().getLinesDX().getStart());
            pointRepository.save(binary.getRail().getLinesDX().getEnd());
            pointRepository.save(binary.getRail().getLinesDX().getCenter());

            lineRepository.save(Arrays.asList(binary.getRail().getLinesDX()));

            pointRepository.save(binary.getRail().getLinesSX().getStart());
            pointRepository.save(binary.getRail().getLinesSX().getEnd());
            pointRepository.save(binary.getRail().getLinesSX().getCenter());

            lineRepository.save(Arrays.asList(binary.getRail().getLinesSX()));

            pointRepository.save(binary.getRail().getTranslationRailFromCenter());
            railRepository.save(binary.getRail());

            // sleeper save

            pointRepository.save(binary.getSleeper().getTranslationSleepersFromCenter());
            pointRepository.save(binary.getSleeper().getTranslationBeetweenSleepers());
            pointRepository.save(binary.getSleeper().getCenter());
            railwaySleeperRepository.save(Arrays.asList(binary.getSleeper()));

            for (SupportA supportA : binary.getSupportAList()) {
                pointRepository.save(supportA.getCenter());
                pointRepository.save(supportA.getTranslationSupportAFromCenter());
            }
            supportARepository.save(binary.getSupportAList());
            for (SupportB supportB : binary.getSupportBList()) {
                pointRepository.save(supportB.getCenter());
                pointRepository.save(supportB.getTranslationSupportBFromCenter());
            }
            supportBRepository.save(binary.getSupportBList());
            for (SupportL supportL : binary.getSupportLList()) {
                pointRepository.save(supportL.getCenter());
                pointRepository.save(supportL.getTranslationSupportLFromCenter());
            }
            supportLRepository.save(binary.getSupportLList());
            inventaryBinaryRepository.save(binary.getInventaryBinary());
            binaryRepository.save(binary);
        }
        prototype = prototypeRepository.save(prototype);
        //binaryRepository.save(binaries);
        return prototype;
    }



    //restituisco prototipo da mmodificare
    @RequestMapping(value = "/prototype/create/{id_prototype}", method = RequestMethod.GET)
    public String greetingRoot2(Model model,@PathVariable("id_prototype") long id_p) {

        model.addAttribute("prototype", prototypeRepository.findOne(id_p));

        return "Prototype/inventaryPrototype";
    }
    // modifico i dati che che l'user modifica e salvo nel db
    //
    @RequestMapping(value = "/prototype/create/{id_prototype}", method = RequestMethod.POST)
    public String createPrototype(Model model, @PathVariable("id_prototype") long idPrototype, @RequestParam("nameBinary") String[] namesBinary,
                                  @RequestParam("supportAnumber") String[] numberS) {
        int[] number =  new int[numberS.length];
        for (int i = 0; i < numberS.length; i++) {
            number[i] = Integer.parseInt(numberS[i]);
        }

        Prototype p = prototypeRepository.findOne(idPrototype);
        for (int i = 0; i < p.getBinaries().size(); i++) {
            p.getBinaries().get(i).getInventaryBinary().setNameBinary(namesBinary[i]);
            p.getBinaries().get(i).setName(namesBinary[i]);
            p.getBinaries().get(i).getInventaryBinary().setNumberSupportA(number[i]);
            p.getBinaries().get(i).getInventaryBinary().setNumberSupportB(number[i]*InventaryBinary.NUMBEROFSUPPORTBNECESSARY);
            p.getBinaries().get(i).getInventaryBinary().setNumberSupportL(number[i]*InventaryBinary.NUMBEROFSUPPORTLNECESSARY);
            if(p.getBinaries().get(i).getSupportAList().size() != number[i]){
                SupportA[] supportAS = new SupportA[number[i]];
                SupportB[] supportBS = new SupportB[number[i]];
                SupportL[] supportLS = new SupportL[number[i]];
                for (int j = 0; j < number[i]; j+=2) {
                    supportAS[j] = new SupportA(p.getBinaries().get(i).getName()+"_supportA",p.getBinaries().get(i).getSupportAList().get(0));
                    supportAS[j+1] = new SupportA(p.getBinaries().get(i).getName()+"_supportA",p.getBinaries().get(i).getSupportAList().get(1));
                    supportBS[j] = new SupportB(p.getBinaries().get(i).getName()+"_supportB",p.getBinaries().get(i).getSupportBList().get(0));
                    supportBS[j+1] = new SupportB(p.getBinaries().get(i).getName()+"_supportB",p.getBinaries().get(i).getSupportBList().get(1));
                    supportLS[j] = new SupportL(p.getBinaries().get(i).getName()+"_supportL",p.getBinaries().get(i).getSupportLList().get(0));
                    supportLS[j+1] = new SupportL(p.getBinaries().get(i).getName()+"_supportL",p.getBinaries().get(i).getSupportLList().get(1));
                }
                p.getBinaries().get(i).setSupportAList(new ArrayList<>(Arrays.asList(supportAS)));
                p.getBinaries().get(i).setSupportBList(new ArrayList<>(Arrays.asList(supportBS)));
                p.getBinaries().get(i).setSupportLList(new ArrayList<>(Arrays.asList(supportLS)));
            }else{
                for (SupportB supportB : p.getBinaries().get(i).getSupportBList()) {
                    supportB.setName(p.getBinaries().get(i).getName()+"_supportB");
                }
                for (SupportA supportA : p.getBinaries().get(i).getSupportAList()) {
                    supportA.setName(p.getBinaries().get(i).getName()+"_supportA");
                }
                for (SupportL supportL : p.getBinaries().get(i).getSupportLList()) {
                    supportL.setName(p.getBinaries().get(i).getName()+"_supportL");
                }
            }
            p.getBinaries().get(i).getSleeper().setName(p.getBinaries().get(i).getName()+"_sleeper");
            p.getBinaries().get(i).getRail().setName(p.getBinaries().get(i).getName()+"_rail");
            p.getBinaries().get(i).setCamera(p.getBinaries().get(i).getName()+"_camera");
            p.getBinaries().get(i).getGrids().get(0).setName(p.getBinaries().get(i).getName()+ "_grid_sx");
            p.getBinaries().get(i).getGrids().get(1).setName(p.getBinaries().get(i).getName()+ "_grid_center");
            p.getBinaries().get(i).getGrids().get(2).setName(p.getBinaries().get(i).getName()+ "_grid_dx");

        }
        p = savePrototype(p);

        model.addAttribute("prototype", p);

        return "Prototype/createPrototype";
    }
        //carico le immagini salvo il path nella classe appropriata del prototipo e salvo l'immagine
    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    ResponseAjax uploadImage(@RequestParam("img_data") String encodedImage, @RequestParam("name_file") String nameImage,
                             @RequestParam("id_prototype") long id_prototype, @RequestParam("type") String type,
                             @RequestParam("binary") int binary_index) {
        //System.out.println(encodedImage);

        String[] parts = encodedImage.split(",");
        String imageString = parts[1];

        BufferedImage image = null;
        byte[] imageByte;
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String path = getPathFile(nameImage+".jpg","image");
        File outputfile = new File(path);
        try {
            ImageIO.write(image, "jpg", outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Prototype p = prototypeRepository.findOne(id_prototype);
        switch (type) {
            case "supportA":
                for (SupportA supportA : p.getBinaries().get(binary_index).getSupportAList()) {
                    supportA.setPath(path);
                }
                supportARepository.save(p.getBinaries().get(binary_index).getSupportAList());
                break;
            case "grids":
                for (GridPolyline gridPolyline : p.getBinaries().get(binary_index).getGrids()) {
                    gridPolyline.setPath(path);
                }
                gridPolylineRepository.save(p.getBinaries().get(binary_index).getGrids());
                break;
            case "supportB":
                for (SupportB supportB: p.getBinaries().get(binary_index).getSupportBList()) {
                    supportB.setPath(path);
                }
                supportBRepository.save(p.getBinaries().get(binary_index).getSupportBList());
                break;
            case "supportL":
                for (SupportL supportL: p.getBinaries().get(binary_index).getSupportLList()) {
                    supportL.setPath(path);
                }
                supportLRepository.save(p.getBinaries().get(binary_index).getSupportLList());
                break;

        }
        prototypeRepository.save(p);


        return new ResponseAjax("ok");
    }
    //salvare la scena
    @RequestMapping(value = "/saveScene", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    ResponseAjax uploadImage(@RequestParam("file") String fileJson,
                             @RequestParam("id_prototype") long id_prototype) {
        Prototype p = prototypeRepository.findOne(id_prototype);

        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toString() + "src/main/resources/static/sceneJson/"+p.getName()+"_"+p.getId()+".txt";
        File file = new File(path);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write (fileJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
        p.setPathScene(path);
        savePrototype(p);

        prototypeRepository.save(p);

        return new ResponseAjax("ok");
    }

    private Point calculateCenter(Bounds bounds) {
        Point centerXY = new Point();
        centerXY.setX(bounds.getMinimumX() + (bounds.getMaximumX() - bounds.getMinimumX()) / 2);
        centerXY.setY(bounds.getMinimumY() + (bounds.getMaximumY() - bounds.getMinimumY()) / 2);
        return centerXY;

    }
        //creo le classi binarytrain e metto in coppia i vari oggetti per completare il binario e i vari supporti
    private List<BinaryTrain> createBinary(List<Line> railsOrdered, List<GridPolyline> gridPolylines) {
        List<BinaryTrain> binaries = new ArrayList<>();
        int numberOfBinaries = railsOrdered.size() / 2;
        for (int i = 0; i < numberOfBinaries; i++) {
            Line railsSX = railsOrdered.get(i * 2);
            Line railsDX = railsOrdered.get(i * 2 + 1);
            Point centerRails = new Point();
            centerRails.setX((railsSX.getCenter().getX() + railsDX.getCenter().getX()) / 2);
            centerRails.setY((railsSX.getCenter().getY() + railsDX.getCenter().getY()) / 2);
            Rail r = new Rail("Binary" + i + "_rail", railsSX, railsDX, centerRails);
            ArrayList<GridPolyline> grids = new ArrayList<>(searchNearestGrid(gridPolylines, centerRails));

            RailwaySleeper sleeper = createRailwaySlepper(grids.get(1).getCenter());
            for (GridPolyline grid : grids) {
                grid.setName("Binary" + i + "_" + grid.getName());
            }

            sleeper.setName("Binary" + i + "_" + sleeper.getName());
            ArrayList<SupportA> supportsA = new ArrayList<>(createSupportA(grids, r));
            for (SupportA support : supportsA) {
                support.setName("Binary" + i + "_" + support.getName());
            }
            ArrayList<SupportB> supportsB = new ArrayList<>(createSupportB(r,supportsA));
            for (SupportB support : supportsB) {
                support.setName("Binary" + i + "_" + support.getName());
            }
            ArrayList<SupportL> supportsL = new ArrayList<>(createSupportL(r,supportsA));
            for (SupportL support : supportsL) {
                support.setName("Binary" + i + "_" + support.getName());
            }

            BinaryTrain binaryTrain = new BinaryTrain("Binary" + i, r, grids, sleeper, supportsA,supportsB,supportsL);
            binaries.add(binaryTrain);
            if (i > 0) {
                Point translation = new Point();
                translation.setY(binaryTrain.getGrids().get(1).getCenter().getY() - binaries.get(0).getGrids().get(1).getCenter().getY());
                translation.setX(binaryTrain.getGrids().get(1).getCenter().getX() - binaries.get(0).getGrids().get(1).getCenter().getX());
                binaryTrain.setTranslationFromOtherBinary(translation);
            } else
                binaryTrain.setTranslationFromOtherBinary(new Point(0, 0));

        }
        return binaries;
    }


    private List<SupportL> createSupportL(Rail r, ArrayList<SupportA> supportsA) {
        int numberOfSupport = supportsA.size();

        SupportL[] supportsL = new SupportL[numberOfSupport];
        for (int i = 0; i < numberOfSupport; i += 2) {
            supportsL[i] = new SupportL("supportL", r.getCenter());
            supportsL[i + 1] = new SupportL("supportL", r.getCenter());
        }
        return Arrays.asList(supportsL);
    }


    private List<SupportA> createSupportA(ArrayList<GridPolyline> grids, Rail r) {
        double heightGrids = (grids.get(1).getMax().getY() - grids.get(1).getMin().getY()) * 100;
        int numberOfSupport = (int) (heightGrids / 60);

        SupportA[] supportsA = new SupportA[numberOfSupport * 2];
        for (int i = 0; i < numberOfSupport * 2; i += 2) {
            supportsA[i] = new SupportA("supportA", r.getCenter());
            supportsA[i + 1] = new SupportA("supportA", r.getCenter());
        }
        return Arrays.asList(supportsA);
    }
    private List<SupportB> createSupportB(Rail r, ArrayList<SupportA> supportAS) {
        int numberOfSupport = supportAS.size();

        SupportB[] supportsB = new SupportB[numberOfSupport];
        for (int i = 0; i < numberOfSupport; i += 2) {
            supportsB[i] = new SupportB("supportB", r.getCenter());
            supportsB[i + 1] = new SupportB("supportB", r.getCenter());
        }
        return Arrays.asList(supportsB);
    }


    private RailwaySleeper createRailwaySlepper(Point centerRails) {
        RailwaySleeper railwaySleeper;
        railwaySleeper = new RailwaySleeper("sleeper", new Point(centerRails.getX(), centerRails.getY() +.1));
        return railwaySleeper;
    }

        //ordino le griglie con una a destra e una sistra e una tra i binari
    private List<GridPolyline> searchNearestGrid(List<GridPolyline> gridPolylines, Point centerRails) {
        GridPolyline[] gridPolylinesArray = new GridPolyline[3];
        List<GridPolyline> gridPolylinesTemp = new ArrayList<>(gridPolylines);
        double distanceMin = Double.MAX_VALUE;
        GridPolyline tempGrid = null;
        for (int j = 0; j < gridPolylinesTemp.size(); j++) {
            double distance = distanceTwoPoint(gridPolylinesTemp.get(j).getCenter(), centerRails);
            if (distance < distanceMin) {
                distanceMin = distance;
                tempGrid = gridPolylinesTemp.get(j);
            }
        }
        gridPolylinesArray[1] = new GridPolyline(tempGrid);
        gridPolylinesTemp.remove(tempGrid);
        //sinistra
        tempGrid = null;
        distanceMin = Double.MAX_VALUE;
        for (int j = 0; j < gridPolylinesTemp.size(); j++) {
            if(gridPolylinesTemp.get(j).getCenter().getX() < centerRails.getX()) {
                double distance = distanceTwoPoint(gridPolylinesTemp.get(j).getCenter(), centerRails);
                if (distance < distanceMin) {
                    distanceMin = distance;
                    tempGrid = gridPolylinesTemp.get(j);
                }
            }
        }
        gridPolylinesArray[0] = new GridPolyline(tempGrid);
        gridPolylinesTemp.remove(tempGrid);
        //destra
        tempGrid = null;
        distanceMin = Double.MAX_VALUE;
        for (int j = 0; j < gridPolylinesTemp.size(); j++) {
            if(gridPolylinesTemp.get(j).getCenter().getX() > centerRails.getX()) {
                double distance = distanceTwoPoint(gridPolylinesTemp.get(j).getCenter(), centerRails);
                if (distance < distanceMin) {
                    distanceMin = distance;
                    tempGrid = gridPolylinesTemp.get(j);
                }
            }
        }
        gridPolylinesArray[2] = new GridPolyline(tempGrid);
        gridPolylinesTemp.remove(tempGrid);

        gridPolylinesArray[0].setName(gridPolylinesArray[0].getName() + "_sx");
        gridPolylinesArray[1].setName(gridPolylinesArray[1].getName() + "_center");
        gridPolylinesArray[2].setName(gridPolylinesArray[2].getName() + "_dx");

        return Arrays.asList(gridPolylinesArray);
    }

    /**
     * ordinamento delle line che raffigurano le rotaie
     **/
    private List<Line> orderRails(List<Line> linesR) {
        List<Line> orderedRails = new ArrayList<>();
        List<Line> temp = new ArrayList<>(linesR);
        double minX = Double.MAX_VALUE;
        Line minLine = null;
        for (int i = 0; i < linesR.size(); i++) {
            for (Line line1 : temp) {
                if (line1.getCenter().getX() < minX) {
                    minLine = line1;
                    minX = line1.getCenter().getX();
                }
            }
            temp.remove(minLine);
            orderedRails.add(minLine);
            minLine = null;
            minX = Double.MAX_VALUE;

        }

        return orderedRails;
    }



    private String uploadFile(String name, MultipartFile file, String folder) {
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                String path = getPathFile(name,folder);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(new File(path)));
                stream.write(bytes);
                stream.close();
                return path;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    private String getPathFile(String nomeFile, String folderFinal){
        String currentUsersHomeDir = System.getProperty("user.home");
        String folderProjectPath = currentUsersHomeDir + File.separator + "PROJECTREX";
        File dirProject = new File(folderProjectPath);
        if(!dirProject.exists()){
            dirProject.mkdir();
        }
        String folderResourcePath = folderProjectPath+File.separator+"resources";
        File dirResources = new File(folderResourcePath);
        if(!dirResources.exists()){
            dirResources.mkdir();
        }
        String finalfolderResourcePath = folderResourcePath+File.separator+folderFinal;
        File dirFolderFinal = new File(finalfolderResourcePath);
        if(!dirFolderFinal.exists()){
            dirFolderFinal.mkdir();
        }
        String filePath = finalfolderResourcePath + File.separator + nomeFile;
        return filePath;
    }
    //cancello i path delle immagini assegnate aigli oggetti del binario
    @RequestMapping(value = "/clearScreen/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseAjax clearScreen(@PathVariable("id") long id_prototype) {
        Prototype p = prototypeRepository.findOne(id_prototype);
        for (BinaryTrain binaryTrain : p.getBinaries()) {
            for (SupportL supportL : binaryTrain.getSupportLList()) {
                supportL.setPath("");
            }
            for (SupportA supportA : binaryTrain.getSupportAList()) {
                supportA.setPath("");
            }
            for (SupportB supportB : binaryTrain.getSupportBList()) {
                supportB.setPath("");}
            for (GridPolyline grid : binaryTrain.getGrids()) {
                grid.setPath("");
            }
        }
        savePrototype(p);

        return new ResponseAjax("ok");
    }
    //creo il file pdf
    //con l'utilizzo della classe  PDFCREATE
    @RequestMapping(value = "/createpdf/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseAjax createPDF(@PathVariable("id") long id_prototype) {
        Prototype p = prototypeRepository.findOne(id_prototype);
        PDFUtility pdfUtility = new PDFUtility();


        for (BinaryTrain binaryTrain : p.getBinaries()) {

            //support A
            String path =getPathFile("supportA_image2.jpg", "image_default");
            PagePDF pagePDFSupportA = new PagePDF();
            pagePDFSupportA.getPaths().add(path);
            String pathViti  =getPathFile("white.jpg", "image_default");
            pagePDFSupportA.getPaths().add(pathViti);

            pagePDFSupportA.getPaths().add(binaryTrain.getSupportAList().get(0).getPath());
            String pathMontaggio = getPathFile("supportA_montaggio.jpg", "image_default");
            pagePDFSupportA.getPaths().add(pathMontaggio);
            pagePDFSupportA.setText("istruzioni montaggio supporto a");
            pagePDFSupportA.setInventaryBinary(binaryTrain.getInventaryBinary());
            pagePDFSupportA.setNumberOfPiece(binaryTrain.getInventaryBinary().getNumberSupportA());
            pagePDFSupportA.setType("supportA");
            pdfUtility.getPages().add(pagePDFSupportA);

            //support B
            path =getPathFile("supportB_image2.jpg", "image_default");
            PagePDF pagePDFSupportB = new PagePDF();
            pagePDFSupportB.getPaths().add(path);
            pathViti  = getPathFile("viti.jpg", "image_default");
            pagePDFSupportB.getPaths().add(pathViti);
            /*for (String supportBPolylinePaths : binaryTrain.getSupportBList()[0].getPaths()) {
                pagePDFSupportB.getPaths().add(supportBPolylinePaths);
            }*/
            pagePDFSupportB.getPaths().add(binaryTrain.getSupportBList().get(0).getPath());
            pathMontaggio =getPathFile("supportB_montaggio.jpg", "image_default");
            pagePDFSupportB.getPaths().add(pathMontaggio);
            pagePDFSupportB.setText("istruzioni montaggio supporto b");
            pagePDFSupportB.setInventaryBinary(binaryTrain.getInventaryBinary());
            pagePDFSupportB.setNumberOfPiece(binaryTrain.getInventaryBinary().getNumberSupportB());
            pagePDFSupportB.setType("supportB");
            pdfUtility.getPages().add(pagePDFSupportB);

            //support L
            path =getPathFile("supportL_image2.jpg", "image_default");
            PagePDF pagePDFSupportL = new PagePDF();
            pagePDFSupportL.getPaths().add(path);
            pathViti  =getPathFile("viti.jpg", "image_default");
            pagePDFSupportL.getPaths().add(pathViti);
            /*for (String supportLPolylinePaths : binaryTrain.getSupportLList()[0].getPaths()) {
                pagePDFSupportL.getPaths().add(supportLPolylinePaths);
            }*/
            pagePDFSupportL.getPaths().add(binaryTrain.getSupportLList().get(0).getPath());
            pathMontaggio = getPathFile("supportL_montaggio.jpg", "image_default");
            pagePDFSupportL.getPaths().add(pathMontaggio);
            pagePDFSupportL.setText("istruzioni montaggio supporto L");
            pagePDFSupportL.setInventaryBinary(binaryTrain.getInventaryBinary());
            pagePDFSupportL.setNumberOfPiece(binaryTrain.getInventaryBinary().getNumberSupportL());
            pagePDFSupportL.setType("supportL");
            pdfUtility.getPages().add(pagePDFSupportL);

            // grids
            PagePDF pagePDFGrids = new PagePDF();

            path = getPathFile("grid_image2.jpg", "image_default");
            pagePDFGrids.getPaths().add(path);
            pathViti  = getPathFile("viti_grid.jpg", "image_default");
            pagePDFGrids.getPaths().add(pathViti);
            pagePDFGrids.getPaths().add(binaryTrain.getGrids().get(0).getPath());

            pathMontaggio = getPathFile("grid_montaggio.jpg", "image_default");
            pagePDFGrids.getPaths().add(pathMontaggio);
            pagePDFGrids.setText("istruzioni per montaggio griglia");
            pagePDFGrids.setInventaryBinary(binaryTrain.getInventaryBinary());
            pagePDFGrids.setNumberOfPiece(3);
            pagePDFGrids.setType("grids");
            pdfUtility.getPages().add(pagePDFGrids);
        }
        PDFCreate pdf = new PDFCreate();
        pdf.createPDF(p, p.getName(), pdfUtility);

        return new ResponseAjax("ok");
    }


    private double distanceTwoPoint(Point a, Point b) {
        return Math.sqrt(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
    }


}
