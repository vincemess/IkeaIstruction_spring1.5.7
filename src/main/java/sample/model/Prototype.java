package sample.model;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 01/06/17.
 */

@Entity
public class Prototype {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    String name;
    @OneToMany(cascade=CascadeType.ALL)
    List<BinaryTrain> binaries = new ArrayList<>();
    String nrModel;
    String nrArticle;

    String client;
    String pathScene;

    public Prototype() {
    }

    public Prototype(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPathScene() {
        return pathScene;
    }

    public void setPathScene(String pathScene) {
        this.pathScene = pathScene;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BinaryTrain> getBinaries() {
        return binaries;
    }

    public void setBinaries(List<BinaryTrain> binaries) {
        this.binaries = binaries;
    }

    public String getNrModel() {
        return nrModel;
    }

    public void setNrModel(String nrModel) {
        this.nrModel = nrModel;
    }

    public String getNrArticle() {
        return nrArticle;
    }

    public void setNrArticle(String nrArticle) {
        this.nrArticle = nrArticle;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
