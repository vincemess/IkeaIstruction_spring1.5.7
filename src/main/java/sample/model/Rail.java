package sample.model;


import org.springframework.security.access.method.P;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 19/06/17.
 */
@Entity
public class Rail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    @OneToOne(cascade=CascadeType.ALL)
    private Line linesSX = new Line();
    @OneToOne(cascade=CascadeType.ALL)
    private Line linesDX = new Line();
    @OneToOne(cascade=CascadeType.ALL)
    private Point center;

    //0 .. sx 1 .. dx
    double[] angle = new double[2];


    @OneToMany
    List<Point> translationRailFromCenter;

    public Rail(String name, Line linesSX, Line linesDX, Point center) {
        this.name = name;
        this.linesSX = linesSX;
        this.linesDX = linesDX;
        this.center = center;
    }

    public Rail() {
    }

    public double[] getAngle() {
        return angle;
    }

    public void setAngle(double[] angle) {
        this.angle = angle;
    }

    public List<Point> getTranslationRailFromCenter() {
        return translationRailFromCenter;
    }

    public void setTranslationRailFromCenter(ArrayList translationRailFromCenter) {
        this.translationRailFromCenter = translationRailFromCenter;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Line getLinesSX() {
        return linesSX;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLinesSX(Line linesSX) {
        this.linesSX = linesSX;
    }

    public Line getLinesDX() {
        return linesDX;
    }

    public void setLinesDX(Line linesDX) {
        this.linesDX = linesDX;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }
}
