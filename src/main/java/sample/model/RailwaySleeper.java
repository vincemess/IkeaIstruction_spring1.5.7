package sample.model;


import javax.persistence.*;

/**
 * Created by vincenzo on 19/06/17.
 */
@Entity
public class RailwaySleeper {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    @OneToOne(cascade=CascadeType.ALL)
    private Point center;
    @OneToOne(cascade=CascadeType.ALL)
    Point translationSleepersFromCenter;
    @OneToOne(cascade=CascadeType.ALL)
    Point translationBeetweenSleepers;


    public RailwaySleeper(String name, Point center) {
        this.name = name;
        this.center = center;
    }

    public RailwaySleeper() {
    }

    public Point getTranslationBeetweenSleepers() {
        return translationBeetweenSleepers;
    }

    public void setTranslationBeetweenSleepers(Point translationBeetweenSleepers) {
        this.translationBeetweenSleepers = translationBeetweenSleepers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getTranslationSleepersFromCenter() {
        return translationSleepersFromCenter;
    }

    public void setTranslationSleepersFromCenter(Point translationSleepersFromCenter) {
        this.translationSleepersFromCenter = translationSleepersFromCenter;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }
}
