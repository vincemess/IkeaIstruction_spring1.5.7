package sample.model;

import javax.persistence.*;

/**
 * Created by vincenzo on 28/06/17.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Support {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  long id;
    @OneToOne(cascade=CascadeType.ALL)
    Point center;
    String name;
    String path = "";


    public Support(String name, Point center) {
        this.name = name;
        this.center = center;
    }

    public Support() {
    }

    public Support(Support supportA) {
        this.center = new Point(supportA.getCenter());
        this.name = supportA.getName();
        this.path = supportA.getPath();
    }

    public Support(String name, Support supportA) {
        this.center = new Point(supportA.getCenter());
        this.name = name;
        this.path = supportA.getPath();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }
}
