package sample.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 28/06/17.
 */
@Entity
public class SupportA extends Support {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  long id;

    @OneToOne(cascade=CascadeType.ALL)
    Point translationSupportAFromCenter;


    public SupportA(String name, Point center) {
        this.name = name;
        this.center = center;
    }

    public SupportA() {
    }

    public SupportA(SupportA supportA) {
        this.center = new Point(supportA.getCenter());
        this.name = supportA.getName();
        this.path = supportA.getPath();
        this.translationSupportAFromCenter = new Point(supportA.getTranslationSupportAFromCenter());
    }

    public SupportA(String name, SupportA supportA) {
        this.center = new Point(supportA.getCenter());
        this.name = name;
        this.path = supportA.getPath();
        this.translationSupportAFromCenter = new Point(supportA.getTranslationSupportAFromCenter());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Point getTranslationSupportAFromCenter() {
        return translationSupportAFromCenter;
    }

    public void setTranslationSupportAFromCenter(Point translationSupportAFromCenter) {
        this.translationSupportAFromCenter = translationSupportAFromCenter;
    }
}
