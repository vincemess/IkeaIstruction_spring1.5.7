package sample.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 28/06/17.
 */
@Entity
public class SupportL extends Support {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  long id;

    @OneToOne(cascade=CascadeType.ALL)
    Point translationSupportLFromCenter;

    public static final int NECESSARYSCREWS = 2;


    public SupportL(String name, Point center) {
        this.name = name;
        this.center = center;
    }

    public SupportL() {
    }

    public SupportL(SupportL supportL) {
        this.center = new Point(supportL.getCenter());
        this.name = supportL.getName();
        this.path = supportL.getPath();
        this.translationSupportLFromCenter = new Point(supportL.getTranslationSupportLFromCenter());
    }

    public SupportL(String name, SupportL supportL) {
        this.center = new Point(supportL.getCenter());
        this.name = supportL.getName();
        this.path = supportL.getPath();
        this.translationSupportLFromCenter = new Point(supportL.getTranslationSupportLFromCenter());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Point getTranslationSupportLFromCenter() {
        return translationSupportLFromCenter;
    }

    public void setTranslationSupportLFromCenter(Point translationSupportLFromCenter) {
        this.translationSupportLFromCenter = translationSupportLFromCenter;
    }
}
