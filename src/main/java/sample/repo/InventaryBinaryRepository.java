package sample.repo;

/**
 * Created by vincenzo on 03/07/17.
 */

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.BinaryTrain;
import sample.model.InventaryBinary;

@Repository
public interface InventaryBinaryRepository extends CrudRepository<InventaryBinary, Long> {


}
