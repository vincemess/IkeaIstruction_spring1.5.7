package sample.repo;

/**
 * Created by vincenzo on 03/07/17.
 */

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Prototype;

@Repository
public interface PrototypeRepository extends CrudRepository<Prototype, Long> {


}
