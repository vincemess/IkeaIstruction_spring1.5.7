var objects = [];
var objectsToRender = [];

var container;
var camera, controls, scene, renderer;
var gridHelper;
var transfControl;
var raycaster, mouse;
var viewer = true;
var dragControls;
var SELECTED_OBJECT;
var OBJECT_LOADED = [];
var scaleFactor = 50;
var OBJECT_TO_ADD;
var addObject = false;
var removeObject = false;
var is_render = false;
var light;
var camera_position = [];
var clock;

var moveForward = false;
var moveBackward = false;
var moveLeft = false;
var moveRight = false;
var prevTime = performance.now();
var velocity;
var first = true;

var binaryWiewer =0;

var SHADOW_MAP_WIDTH = 2048, SHADOW_MAP_HEIGHT = 1024;


var MYSCENE = {

    init: function () {
        console.log('init');
        clock = new THREE.Clock();
        velocity = new THREE.Vector3();

        container = document.getElementById('myEmbeddedScene');

        camera = new THREE.PerspectiveCamera(45, container.clientWidth / container.clientWidth, 1, 10000);

        camera.position.z = 0;
        camera.position.x = 90;
        camera.position.y = 80;
        camera.lookAt( new THREE.Vector3(-prototype.binaries[binaryWiewer].center_binary[0],0,prototype.binaries[binaryWiewer].center_binary[1]));
        camera_position.push(camera.position.x);
        camera_position.push(camera.position.y);
        camera_position.push(camera.position.z);


        // world
        scene = new THREE.Scene();
        gridHelper = new THREE.GridHelper(10000, 1000);
        scene.add(gridHelper);

        var center_scene = (prototype.binaries[0].grids[1].center.x + prototype.binaries[prototype.binaries.length - 1].grids[1].center.x) / 2 * scaleFactor;

        light = new THREE.SpotLight(0xffffff, 1, 0, Math.PI / 2);
        light.position.set(center_scene, 1000, -500);
        light.target.position.set(center_scene, 0, 0);
        light.castShadow = true;
        light.shadow = new THREE.LightShadow(camera);
        light.shadow.bias = 0.0001;
        light.shadow.mapSize.width = SHADOW_MAP_WIDTH;
        light.shadow.mapSize.height = SHADOW_MAP_HEIGHT;
        scene.add(light);


        // renderer
        renderer = new THREE.WebGLRenderer({
            preserveDrawingBuffer: true,
            antialias: true        // required to support .toDataURL()
        });
        renderer.setClearColor(0xdddddd, 1);
        renderer.setPixelRatio(window.devicePixelRatio);

        renderer.setSize(container.clientWidth, container.clientWidth);


        container.appendChild(renderer.domElement);

        dragControls = new THREE.DragControls(objects, camera, renderer.domElement);
        dragControls.addEventListener('dragstart', function (event) {
        });
        dragControls.addEventListener('dragend', function (event) {
        });


        transfControl = new THREE.TransformControls(camera, renderer.domElement);
        transfControl.addEventListener('change', render);
        scene.add(transfControl);
        //
        window.addEventListener('resize', onWindowResize, false);
        document.addEventListener('keydown', onKeyDown, false);
        document.addEventListener('keyup', onKeyUp, false);
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();


        container.addEventListener('mousemove', onMouseMove, false);
        container.addEventListener('mousedown', onDocumentMouseDown, false);

        render();


        // listener mouse
        function onMouseMove(event) {

            event.preventDefault();

            var rect = renderer.domElement.getBoundingClientRect();

            mouse.x = ((event.clientX - rect.left) / rect.width ) * 2 - 1;
            mouse.y = -( (event.clientY - rect.top) / rect.height ) * 2 + 1;
            raycaster.setFromCamera(mouse, camera);

            var intersects = raycaster.intersectObjects(objects, true);

            if (intersects.length > 0) {
                SELECTED_OBJECT = intersects[0].object;
                //console.log(SELECTED_OBJECT.position);
            }

        }

        function onDocumentMouseDown(event) {
            event.preventDefault();
            if (!viewer) {
                transfControl.detach();
                transfControl.attach(SELECTED_OBJECT);
            }
            if (addObject) {
                var object = OBJECT_TO_ADD.clone();
                object.position.z = mouse.y;
                OBJECT_LOADED.push(object);
                addObject = false;
            }

        }

        // listener tasti
        function onKeyUp(event) {
            switch (event.keyCode) {
                case 87: // w
                    //moveForward = false;
                    break;
                case 65: // a
                    //moveLeft = false;
                    break;
                case 83: // s
                    //moveBackward = false;
                    break;
                case 68: // d
                    //moveRight = false;
                    break;

            }
        }

        function onKeyDown() {
            switch (event.keyCode) {
                case 79: /*O*/

                    break;
                case 80: /*P*/

                    break;
                case 38: // up
                    light.position.z += 10;
                    break;
                case 87: // w
                    camera.position.y+=10;
                    camera.lookAt( new THREE.Vector3(-prototype.binaries[binaryWiewer].center_binary[0],0,prototype.binaries[binaryWiewer].center_binary[1]));
                    console.log(camera.position)
                    break;
                case 37: // left
                    light.position.x += 10;
                    break;
                case 65: // a
                    camera.position.x-=10;
                    camera.lookAt( new THREE.Vector3(-prototype.binaries[binaryWiewer].center_binary[0],0,prototype.binaries[binaryWiewer].center_binary[1]));
                    console.log(camera.position)
                    break;
                case 40: // down
                    light.position.z -= 10;
                    break;
                case 83: // s
                    camera.position.y-=10;
                    camera.lookAt( new THREE.Vector3(-prototype.binaries[binaryWiewer].center_binary[0],0,prototype.binaries[binaryWiewer].center_binary[1]));
                    console.log(camera.position)

                    break;
                case 39: // right
                    light.position.x -= 10;
                    break;
                case 68: // d
                    camera.position.x+=10;
                    camera.lookAt( new THREE.Vector3(-prototype.binaries[binaryWiewer].center_binary[0],0,prototype.binaries[binaryWiewer].center_binary[1]));
                    console.log(camera.position)

                    break;
                case 81: // q
                    camera.position.z-=10;
                    camera.lookAt( new THREE.Vector3(-prototype.binaries[binaryWiewer].center_binary[0],0,prototype.binaries[binaryWiewer].center_binary[1]));
                    console.log(camera.position)

                    break;
                case 69: // E
                    camera.position.z+=10;
                    camera.lookAt( new THREE.Vector3(-prototype.binaries[binaryWiewer].center_binary[0],0,prototype.binaries[binaryWiewer].center_binary[1]));
                    console.log(camera.position)

                    break;
                case 84: // T
                    transfControl.setMode("translate");
                    viewer = false;
                    dragControls.enabled = false;
                    break;
                case 82: // R
                    transfControl.setMode("rotate");
                    viewer = false;
                    dragControls.enabled = false;
                    break;
                case 89: // y
                    transfControl.setMode("scale");
                    viewer = false;
                    dragControls.enabled = false;
                    break;
                case 187:
                case 107: // +, =, num+
                    transfControl.setSize(transfControl.size + 0.1);
                    viewer = false;
                    dragControls.enabled = false;
                    break;
                case 189:
                case 109: // -, _, num-
                    transfControl.setSize(Math.max(transfControl.size - 0.1, 0.1));
                    dragControls.enabled = false;
                    viewer = false;
                    break;
                case 86: // V
                    transfControl.detach();
                    dragControls.enabled = true;
                    controls.enabled = !controls.enabled;
                    viewer = true;
                    break;
                case 67: // C
                    camera.position.z = 0;
                    camera.position.x = -prototype.binaries[1].center_binary[0];
                    camera.position.y = 800;
                    camera.lookAt( new THREE.Vector3(-prototype.binaries[1].center_binary[0],0,prototype.binaries[1].center_binary[1]));
                    binaryWiewer = 1;

                    break;
                    case 88: // S
                        camera_position = [];
                        camera_position.push(camera.position.x);
                        camera_position.push(camera.position.y);
                        camera_position.push(camera.position.z);

                    break;
            }
        }

        function onWindowResize() {
            camera.aspect = container.clientWidth / container.clientWidth;
            camera.updateProjectionMatrix();
            renderer.setSize(container.clientWidth, container.clientWidth);
            //controls.handleResize();
            render();
        }


    },

    animation: function () {
        animate();
        function animate() {
            requestAnimationFrame(animate);


            var time = performance.now();
            var delta = ( time - prevTime ) / 1000;
            if (moveForward) velocity.z -= 100000.0 * delta;
            if (moveBackward) velocity.z += 100000.0 * delta;
            if (moveLeft) velocity.x -= 100000.0 * delta;
            if (moveRight) velocity.x += 100000.0 * delta;
            camera.translateX(velocity.x * delta);
            camera.translateY(velocity.y * delta);
            camera.translateZ(velocity.z * delta);
            prevTime = time;
            velocity.x = 0;
            velocity.y = 0;
            velocity.z = 0;


            render();
        }

    },
    loadMesh: function (prototype) {
        var numberOfFileToLoad = 5;
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;
        var railwaySleepers;
        var heightSleepers;
        var widthSleepers;
        var heightRails;
        var rails;
        var temp = [];
        var ALL_RAILS;
        var ALL_SLEEPERS;
        var ALL_GRIDS;
        var ALL_SUPPORTA;
        var ALL_SUPPORTB;
        var supportA;
        var supportB;
        for (var k = 0; k < prototype.binaries.length; k++) {
            prototype.binaries[k].center_binary = [];
            prototype.binaries[k].center_binary.push(prototype.binaries[k].translationFromOtherBinary.x);
            prototype.binaries[k].center_binary.push(prototype.binaries[k].translationFromOtherBinary.y);
            prototype.binaries[k].center_binary[0] = prototype.binaries[k].center_binary[0] * scaleFactor;
            prototype.binaries[k].center_binary[1] = prototype.binaries[k].center_binary[1] * scaleFactor;
            prototype.binaries[k].meshFather = new THREE.Object3D();

            prototype.binaries[k].meshFather.position.x = -prototype.binaries[k].center_binary[0];
            prototype.binaries[k].meshFather.position.z = prototype.binaries[k].center_binary[1];
            prototype.binaries[k].meshFather.position.y = 0;

    }
        //carico mesh supportB
        loader.load('/fileMesh/supportB_scale_green.dae', function (collada) {
            supportB = collada.scene.children[0].children[0];
            supportB.geometry.computeBoundingBox();
            supportB.rotation.x = -Math.PI / 2;
            supportB.rotation.y = 0;
            supportB.rotation.z = -Math.PI / 2;
            supportB.geometry.computeBoundingBox();
            ALL_SUPPORTB = createSupportB(supportB);
            //heightSleepers =  railwaySleepers.geometry.boundingBox.max.z-railwaySleepers.geometry.boundingBox.min.z;
            temp.push(supportB);

            //controlla se ha caricato tutti i file e nel  caso chiama il metodo setPosition(sarà chiamato una volta sola)
            if (temp.length >= numberOfFileToLoad) {
                setPosition( heightSleepers, widthSleepers, heightRails);
            }


        });
        //carico mesh supportL
        loader.load('/fileMesh/supportL_cut_blue.dae', function (collada) {
            var supportL = collada.scene.children[0].children[0];
            createSupportL(supportL);
            temp.push(supportL);
            if (temp.length >= numberOfFileToLoad) {
                setPosition( heightSleepers, widthSleepers, heightRails);
            }


        });
        //carico mesh binario
        loader.load('/fileMesh/binario_scale_transparent.dae', function (collada) {
            rails = collada.scene.children[0].children[0];
            rails.material[1].transparent = true;
            rails.material[1].opacity = 0.8;
            rails.geometry.computeBoundingBox();
            rails.rotation.x = -Math.PI / 2 - 0.028;
            rails.rotation.z = -Math.PI / 2;
            ALL_RAILS = createRails(rails);
            heightRails = rails.geometry.boundingBox.max.y - rails.geometry.boundingBox.min.y;
            temp.push(rails);
            if (temp.length >= numberOfFileToLoad) {
                setPosition( heightSleepers, widthSleepers,  heightRails);
            }


        });
        //carico mesh traversina
        loader.load('/fileMesh/traversina_scaled_transparent.dae', function (collada) {
            railwaySleepers = collada.scene.children[0].children[0];
            railwaySleepers.material[0].transparent = true;
            railwaySleepers.material[0].opacity = 0.8;
            railwaySleepers.rotation.x = -Math.PI / 2;
            railwaySleepers.rotation.y = 0;
            railwaySleepers.rotation.z = -Math.PI / 2;
            railwaySleepers.scale.set(1.1, 1.1, 1.1);
            railwaySleepers.geometry.computeBoundingBox();
            ALL_SLEEPERS = createRailwaySleepers(railwaySleepers);
            heightSleepers = (railwaySleepers.geometry.boundingBox.max.z - railwaySleepers.geometry.boundingBox.min.z) * 1.1;

            widthSleepers = (railwaySleepers.geometry.boundingBox.max.x - railwaySleepers.geometry.boundingBox.min.x) * 1.1;
            temp.push(railwaySleepers);
            if (temp.length >= numberOfFileToLoad) {
                setPosition( heightSleepers, widthSleepers,  heightRails);
            }

        });
        //carico mesh supportA
        loader.load('/fileMesh/supportA_scale_red.dae', function (collada) {
            supportA = collada.scene.children[0].children[0];
            supportA.geometry.computeBoundingBox();
            supportA.rotation.x = -Math.PI / 2;
            supportA.rotation.y = 0;
            supportA.rotation.z = -Math.PI / 2;
            supportA.geometry.computeBoundingBox();
            ALL_SUPPORTA = createSupportA(supportA);
            //heightSleepers =  railwaySleepers.geometry.boundingBox.max.z-railwaySleepers.geometry.boundingBox.min.z;
            temp.push(supportA);
            if (temp.length >= numberOfFileToLoad) {

                setPosition( heightSleepers, widthSleepers, heightRails);
            }

        });

        function setPosition( heightSleepers, widthSleepers, heightRails) {
            var railwayLevels = 0.2;
            var railLevels = heightSleepers + railwayLevels;
            var gridLevel = heightRails + railLevels;

            ALL_GRIDS = createGrids(prototype);

            //setto y delle mesh
            for (var i = 0; i < prototype.binaries.length; i++) {

                for(var k = 0; k < prototype.binaries[i].rail.mesh.length; k++){
                    prototype.binaries[i].rail.mesh[k].position.y = railLevels + 0.5;
                }

                for (var k = 0; k < prototype.binaries[i].supportAList.length; k++) {
                    prototype.binaries[i].supportAList[k].mesh.position.y = railLevels;
                }
                for (var k = 0; k <prototype.binaries[i].supportBList.length; k++) {
                    prototype.binaries[i].supportBList[k].mesh.position.y = railLevels;
                }
                for (var k = 0; k <prototype.binaries[i].supportLList.length; k++) {
                }
                for (var k = 0; k <prototype.binaries[i].sleeper.mesh.length; k++) {
                    prototype.binaries[i].sleeper.mesh[k].position.y = railwayLevels;
                }

                OBJECT_LOADED.push(prototype.binaries[i].meshFather);

            }
            //parametri griglie
            for (var i = 0; i < ALL_GRIDS.length; i++) {
                ALL_GRIDS[i].geometry.computeBoundingBox();
                var height_grid = ALL_GRIDS[i].geometry.boundingBox.max.y - ALL_GRIDS[i].geometry.boundingBox.min.y;
                ALL_GRIDS[i].scale.set(scaleFactor, scaleFactor, scaleFactor);
                ALL_GRIDS[i].position.y = gridLevel;
                OBJECT_LOADED.push(ALL_GRIDS[i]);
            }

            //individuo traversine che si trovano sotto la griglia centrale
            var profgridsCenter;
            for (var i = 0; i < prototype.binaries.length; i++) {
                prototype.binaries[i].sleeper.sleppers_bottom_grids = [];
                profgridsCenter = (prototype.binaries[i].grids[1].mesh.geometry.boundingBox.max.y - prototype.binaries[i].grids[1].mesh.geometry.boundingBox.min.y) * scaleFactor / 2;
                for (var j = 0; j < prototype.binaries[i].sleeper.mesh.length; j++) {
                    //profSleepers = (prototype.binaries[i].sleeper.mesh[j].geometry.boundingBox.max.y - prototype.binaries[i].sleeper.mesh[j].geometry.boundingBox.min.y) / 2;
                    /**if (!((prototype.binaries[i].sleeper.mesh[j].position.z + profSleepers < prototype.binaries[i].grids[1].mesh.position.z + profgridsCenter
                     || (prototype.binaries[i].sleeper.mesh[j].position.z - profSleepers > prototype.binaries[i].grids[1].mesh.position.z - profgridsCenter)))) {**/

                    if (((prototype.binaries[i].sleeper.mesh[j].position.z  < prototype.binaries[i].grids[1].mesh.position.z + profgridsCenter
                        && (prototype.binaries[i].sleeper.mesh[j].position.z  > prototype.binaries[i].grids[1].mesh.position.z - profgridsCenter)))) {
                        //prototype.binaries[i].sleeper.mesh[j].visible = false;
                        prototype.binaries[i].sleeper.sleppers_bottom_grids.push(prototype.binaries[i].sleeper.mesh[j]);
                    }
                }


                //ordino le traversine mettendole una il più lontano possibile dall'altra
                var orderedSleeper = [];
                var temp = prototype.binaries[i].sleeper.sleppers_bottom_grids.slice();
                orderedSleeper.push(prototype.binaries[i].sleeper.sleppers_bottom_grids[0]);
                var index = temp.indexOf(prototype.binaries[i].sleeper.sleppers_bottom_grids[0]);
                temp.splice(index, 1);
                var distance;
                var distanceMax = 0;
                var slepperDistanceMax;

                for (var k = 1; k < prototype.binaries[i].sleeper.sleppers_bottom_grids.length; k++) {
                    var a = orderedSleeper[k - 1];
                    distanceMax = 0;
                    for (var j = 0; j < temp.length; j++) {
                        var b = temp[j];
                        distance = Math.sqrt((a.position.x - b.position.x) * (a.position.x - b.position.x) + (a.position.z - b.position.z) * (a.position.z - b.position.z));
                        if (distance > distanceMax) {
                            distanceMax = distance;
                            slepperDistanceMax = b;
                        }
                    }
                    var index = temp.indexOf(slepperDistanceMax);

                    temp.splice(index, 1);
                    orderedSleeper.push(slepperDistanceMax);
                }
                prototype.binaries[i].sleeper.sleppers_bottom_grids = [];

                for (var y = 0; y < orderedSleeper.length; y++) {
                    prototype.binaries[i].sleeper.sleppers_bottom_grids.push(orderedSleeper[y]);
                }
            }

            //definisco la posizione z di tutti i supporti
            var k = 0;
            for (var i = 0; i < prototype.binaries.length; i++) {
                var minus = 1;
                k = 0;
                //TODO change length
                for (var j = 0; j < prototype.binaries[i].supportAList.length; j += 2) {
                    if (k < prototype.binaries[i].sleeper.sleppers_bottom_grids.length) {
                        prototype.binaries[i].supportAList[j].mesh.position.z = prototype.binaries[i].sleeper.sleppers_bottom_grids[k].position.z + widthSleepers / 2 * minus + 0.15;
                        prototype.binaries[i].supportAList[j + 1].mesh.position.z = prototype.binaries[i].sleeper.sleppers_bottom_grids[k].position.z + widthSleepers / 2 * minus + 0.15;
                        prototype.binaries[i].supportAList[j].mesh.rotation.z = Math.PI/2;
                        prototype.binaries[i].supportBList[j].mesh.position.z = prototype.binaries[i].sleeper.sleppers_bottom_grids[k].position.z + widthSleepers / 2 * minus + 0.15;
                        prototype.binaries[i].supportBList[j + 1].mesh.position.z = prototype.binaries[i].sleeper.sleppers_bottom_grids[k].position.z + widthSleepers / 2 * minus + 0.15;

                        prototype.binaries[i].supportLList[j].mesh.position.z = prototype.binaries[i].sleeper.sleppers_bottom_grids[k].position.z + widthSleepers / 2 * minus + 0.15;
                        prototype.binaries[i].supportLList[j + 1].mesh.position.z = prototype.binaries[i].sleeper.sleppers_bottom_grids[k].position.z + widthSleepers / 2 * minus + 0.15;
                        prototype.binaries[i].supportLList[j].mesh.rotation.y = Math.PI/2;
                        prototype.binaries[i].supportLList[j + 1].mesh.rotation.y = -Math.PI/2;

                        prototype.binaries[i].supportAList[j + 1].mesh.geometry.computeBoundingBox();
                        var widthSleeper = prototype.binaries[i].supportAList[j + 1].mesh.geometry.boundingBox.max.y - prototype.binaries[i].supportAList[j + 1].mesh.geometry.boundingBox.min.y;
                        var heightSleeper = prototype.binaries[i].supportAList[j + 1].mesh.geometry.boundingBox.max.x - prototype.binaries[i].supportAList[j + 1].mesh.geometry.boundingBox.min.x;
                        prototype.binaries[i].supportLList[j].mesh.position.x = prototype.binaries[i].supportLList[j].mesh.position.x - widthSleeper/2;
                        prototype.binaries[i].supportLList[j + 1].mesh.position.x = prototype.binaries[i].supportLList[j+1].mesh.position.x + widthSleeper/2 ;
                        prototype.binaries[i].supportLList[j].mesh.position.y += heightSleepers+2;
                        prototype.binaries[i].supportLList[j + 1].mesh.position.y += heightSleepers+2;
                    } else {
                        console.log("error traversine")
                    }
                    k++;
                    minus = minus * -1;
                }
            }

        }

        // creo tutti i supporti L necessari settando la posizione di defaul calcolata il java
        function createSupportL(supportL) {

            for (var k = 0; k < prototype.binaries.length; k++) {
                for (var j = 0; j < prototype.binaries[k].supportLList.length; j++) {
                    var support = supportL.clone();
                    support.name = prototype.binaries[k].supportLList[j].name;
                    support.position.x =  - prototype.binaries[k].supportLList[j].translationSupportLFromCenter.x * scaleFactor;
                    support.position.y = 0.01;
                    support.position.z =  + prototype.binaries[k].supportLList[j].translationSupportLFromCenter.y * scaleFactor;
                    //support.rotation.z =  Math.PI/2;
                    prototype.binaries[k].supportLList[j].mesh = support;
                    prototype.binaries[k].meshFather.add(support);

                }
            }


            return temp;
        }

        // creo tutti le rotaie necessari settando la posizione di defaul calcolata il java
        function createRails(rails) {

            var temp = [];
            var lenghtRails = (rails.geometry.boundingBox.max.x - rails.geometry.boundingBox.min.x);
            for (var k = 0; k < prototype.binaries.length; k++) {
                prototype.binaries[k].rail.mesh = [];

                for (var i = 0; i < 2; i++) {
                    var rails = rails.clone();
                    rails.name = prototype.binaries[k].rail.name;
                    rails.position.x = -prototype.binaries[k].rail.translationRailFromCenter[i].x * scaleFactor;
                    rails.position.y = 0.01;
                    rails.position.z = prototype.binaries[k].rail.translationRailFromCenter[0].y * scaleFactor ;

                    prototype.binaries[k].rail.mesh.push(rails);
                    prototype.binaries[k].meshFather.add(rails);
                    //temp.push(rails);
                }
                prototype.binaries[k].meshFather.rotation.y = -prototype.binaries[k].rail.angle[0];
                temp.push(prototype.binaries[k].meshFather);

            }

            return temp;
        }
        // creo tutti i supporti A necessari settando la posizione di defaul calcolata il java
        function createSupportA(supportA) {
            var temp = [];
            for (var k = 0; k < prototype.binaries.length; k++) {
                for (var j = 0; j < prototype.binaries[k].supportAList.length; j++) {
                    var support = supportA.clone();
                    support.name = prototype.binaries[k].supportAList[j].name;
                    support.position.x =  - prototype.binaries[k].supportAList[j].translationSupportAFromCenter.x * scaleFactor;
                    support.position.y = 0.01;
                    var t = parseInt(j / 2, 10);
                    support.position.z =  + prototype.binaries[k].supportAList[j].translationSupportAFromCenter.y * scaleFactor;

                    prototype.binaries[k].supportAList[j].mesh = support;
                    //temp.push(support);
                    prototype.binaries[k].meshFather.add(support);
                }

            }
            return temp;
        }
        // creo tutti i supporti B necessari settando la posizione di defaul calcolata il java
        function createSupportB(supportB) {
            var temp = [];
            for (var k = 0; k < prototype.binaries.length; k++) {
                for (var j = 0; j < prototype.binaries[k].supportBList.length; j++) {
                    var support = supportB.clone();
                    support.name = prototype.binaries[k].supportBList[j].name;
                    support.position.x =  - prototype.binaries[k].supportBList[j].translationSupportBFromCenter.x * scaleFactor;
                    support.position.y = 0.01;
                    var t = parseInt(j / 2, 10);
                    support.position.z =  + prototype.binaries[k].supportBList[j].translationSupportBFromCenter.y * scaleFactor;

                    prototype.binaries[k].supportBList[j].mesh = support;
                    //temp.push(support);
                    prototype.binaries[k].meshFather.add(support);
                }

            }
            return temp;
        }
        // creo tutte le traversine necessarie settando la posizione di defaul calcolata il java
        function createRailwaySleepers(railwaySleepers) {
            var temp = [];
            for (var k = 0; k < prototype.binaries.length; k++) {
                var sleepers;
                prototype.binaries[k].sleeper.mesh = [];
                for (var i = -1; i < 2; i++) {
                    sleepers = railwaySleepers.clone();
                    sleepers.name = prototype.binaries[k].sleeper.name;
                    sleepers.position.x =  - (prototype.binaries[k].sleeper.translationSleepersFromCenter.x) * scaleFactor;
                    sleepers.position.y = 0.1;
                    sleepers.position.z =  + ((i * prototype.binaries[k].distanceBetweenSleepers) + prototype.binaries[k].sleeper.translationSleepersFromCenter.y)* scaleFactor;
                    //cube.scale.set(1.5, 1.5, 1.5);
                    //temp.push(sleepers);
                    prototype.binaries[k].sleeper.mesh.push(sleepers);
                    //sleepers.rotation.z = Math.PI / 2 - prototype.binaries[k].rail.angle[1];
                    prototype.binaries[k].meshFather.add(sleepers);
                }


            }
            return temp;
        }
        // creo le griglie caricando in una shape le object coordinate dopo di che con extrudeSetting setto lo spessore
        function createGrids(prototype) {
            var visible = [];
            var piece = [];
            for (var k = 0; k < prototype.binaries.length; k++) {
                for (var i = 0; i < prototype.binaries[k].grids.length; i++) {
                    var vertex = [];


                    for (var j = 0; j < prototype.binaries[k].grids[i].objectCoordinates.length; j++) {
                        vertex.push(
                            new THREE.Vector2(-prototype.binaries[k].grids[i].objectCoordinates[j].x, -prototype.binaries[k].grids[i].objectCoordinates[j].y)
                        );

                    }
                    var shape = new THREE.Shape(vertex);

                    var extrudeSettings = {
                        steps: 1,
                        amount: .03,
                        bevelEnabled: false
                    };

                    var geometry1 = new THREE.ExtrudeGeometry(shape, extrudeSettings);
                    var material2 = new THREE.MeshLambertMaterial({color: 0xffff00});
                    material2.side = THREE.DoubleSide;
                    var mesh1 = new THREE.Mesh(geometry1, material2);
                    mesh1.name = prototype.binaries[k].grids[i].name;

                    mesh1.position.x = -prototype.binaries[k].center_binary[0] - prototype.binaries[k].grids[i].translationGridFromCenter.x * scaleFactor;
                    mesh1.position.y = 0.1;
                    mesh1.position.z = prototype.binaries[k].center_binary[1] + prototype.binaries[k].grids[i].translationGridFromCenter.y * scaleFactor;


                    mesh1.rotation.x = -Math.PI / 2;
                    mesh1.rotation.y = 0;
                    mesh1.rotation.z = 0;

                    //mesh1.scale.set(20, 20, 20);

                    mesh1.updateMatrix();
                    if (visible.indexOf(prototype.binaries[k].grids[i].number) == -1) {
                        visible.push(prototype.binaries[k].grids[i].number);
                    } else {
                        mesh1.visible = false;
                    }
                    mesh1.castShadow = true;
                    piece.push(mesh1);
                    prototype.binaries[k].grids[i].mesh = mesh1;
                    mesh1.geometry.computeBoundingBox();


                }
            }
            return piece;
        }
    }
}


function render() {


        //controllo se ci sono vari oggetti da caricare
    if (OBJECT_LOADED.length > 0) {
        for (var j = 0; j < OBJECT_LOADED.length; j++) {
            objects.push(OBJECT_LOADED[j]);
            objectsToRender.push(OBJECT_LOADED[j]);
        }
        OBJECT_LOADED = [];
        if (first) {
            first = false;
        }
    }
    for (var i = 0; i < objects.length; i++) {
        scene.add(objectsToRender[i]);
    }
    is_render = true;

    //controller transformate
    transfControl.update();
    renderer.render(scene, camera);
}
