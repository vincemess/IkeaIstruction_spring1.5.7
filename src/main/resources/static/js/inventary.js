/**
 * Created by vincenzo on 01/06/17.
 */

var booleans = [];
booleans[0] = true;
booleans[1] = true;

$(document).ready(function () {
    $("#error").hide();
});

function checkpair(value,id) {
    var nameError = "#"+"error"+id;
   if(value % 2 == 0){
       $(nameError).hide();
       booleans[0] = true;
       checkbooleans();
   }else{
       $(nameError).show();
       booleans[0] = false;
       checkbooleans();
   }
}
function checkname(value,id) {
    var nameError = "#"+"error"+id+"_name";
    if(value.indexOf(STARTNAMEOFALLMESH) != -1){
        $(nameError).hide();
        booleans[1] = true;
        checkbooleans();
    }else{
        $(nameError).show();
        booleans[1] = false;
        checkbooleans();
    }
}
function checkbooleans() {
    if(booleans[0] && booleans[1]){
        $("#button").prop("disabled",false);
    }else{
        $("#button").prop("disabled",true);
    }
}
