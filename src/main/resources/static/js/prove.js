/*
 var container;
 var camera, controls, scene, renderer;
 var objects = [];
 init();
 animate();
 function init() {
 container = document.createElement( 'myEmbeddedScene' );
 document.body.appendChild( container );
 camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
 camera.position.z = 10;
 controls = new THREE.TrackballControls( camera );
 controls.rotateSpeed = 1.0;
 controls.zoomSpeed = 1.2;
 controls.panSpeed = 0.8;
 controls.noZoom = false;
 controls.noPan = false;
 controls.staticMoving = true;
 controls.dynamicDampingFactor = 0.3;
 scene = new THREE.Scene();
 scene.add( new THREE.AmbientLight( 0x505050 ) );
 var light = new THREE.SpotLight( 0xffffff, 1.5 );
 light.position.set( 0, 500, 2000 );
 light.castShadow = true;
 light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 50, 1, 200, 10000 ) );
 light.shadow.bias = - 0.00022;
 light.shadow.mapSize.width = 2048;
 light.shadow.mapSize.height = 2048;
 scene.add( light );
 var geometry = new THREE.BoxGeometry( 40, 40, 40 );
 for ( var i = 0; i < 10; i ++ ) {
 var object = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff } ) );
 object.position.x = Math.random() * 1000 - 500;
 object.position.y = Math.random() * 600 - 300;
 object.position.z = Math.random() * 800 - 400;
 object.rotation.x = Math.random() * 2 * Math.PI;
 object.rotation.y = Math.random() * 2 * Math.PI;
 object.rotation.z = Math.random() * 2 * Math.PI;
 object.scale.x = Math.random() * 2 + 1;
 object.scale.y = Math.random() * 2 + 1;
 object.scale.z = Math.random() * 2 + 1;
 object.castShadow = true;
 object.receiveShadow = true;
 scene.add( object );
 objects.push( object );
 }
 renderer = new THREE.WebGLRenderer( { antialias: true } );
 renderer.setClearColor( 0xf0f0f0 );
 renderer.setPixelRatio( window.devicePixelRatio );
 renderer.setSize( window.innerWidth, window.innerHeight );
 renderer.sortObjects = false;
 renderer.shadowMap.enabled = true;
 renderer.shadowMap.type = THREE.PCFShadowMap;
 container.appendChild( renderer.domElement );
 var dragControls = new THREE.DragControls( objects, camera, renderer.domElement );
 dragControls.addEventListener( 'dragstart', function ( event ) { controls.enabled = false; } );
 dragControls.addEventListener( 'dragend', function ( event ) { controls.enabled = true; } );

 //
 window.addEventListener( 'resize', onWindowResize, false );
 }
 function onWindowResize() {
 camera.aspect = window.innerWidth / window.innerHeight;
 camera.updateProjectionMatrix();
 renderer.setSize( window.innerWidth, window.innerHeight );
 }
 //
 function animate() {
 requestAnimationFrame( animate );
 render();

 }
 function render() {
 controls.update();
 renderer.render( scene, camera );
 }



 */



/*
 var clock = new THREE.Clock();
 scene = new THREE.Scene();
 camera = new THREE.PerspectiveCamera( 75, (window.innerWidth*.6)/(window.innerHeight*.6), 0.1, 1000 );
 renderer = new THREE.WebGLRenderer();
 cameraControls = new THREE.TrackballControls(camera);
 cameraControls.target.set(0, 0, 0);

 renderer.setSize( window.innerWidth*.6, window.innerHeight*.6 );
 renderer.setClearColor (0xcccccc, 1);
 $("#myEmbeddedScene").append(renderer.domElement);

 var geometry = new THREE.BoxGeometry( 1, 1, 1 );
 var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
 var cube = new THREE.Mesh( geometry, material );
 scene.add( cube );

 camera.position.z = 5;

 var render = function () {
 requestAnimationFrame( render );
 var delta = clock.getDelta();
 cameraControls.update(delta);

 cube.rotation.x += 0.1;
 cube.rotation.y += 0.1;

 renderer.render(scene, camera);
 };

 render();

 */