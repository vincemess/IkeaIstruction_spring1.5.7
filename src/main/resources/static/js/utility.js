/**
 * Created by vincenzo on 01/06/17.
 */

var STARTNAMEOFALLMESH = "Binary";

$(document).ready(function () {
    $("#createpdf").prop("disabled",true);

    $("#screen").unbind('click').click(function () {
        screenshoot(this.value, this);
        $("#createpdf").prop("disabled",false);
        $("#screen").prop("disabled",true);
    })
    $("#createpdf").unbind('click').click(function () {
        createPDF();
        $("#createpdf").prop("disabled",true);
    })
    $("#clearScreen").unbind('click').click(function () {
        clearScreen();
        $("#createpdf").prop("disabled",true);
        $("#screen").prop("disabled",false);
    })
});


function screenshoot(value, object) {
    saveAsImage();

}
function clearScreen() {
    $.ajax({
        url: "/clearScreen/" + prototype.id,
        type: "GET",
    }).done(function (data) {
        console.log(data);
    });

}
function createPDF() {
    $.ajax({
        url: "/createpdf/" + prototype.id,
        type: "GET",
    }).done(function (data) {
        console.log(data);
    });

}
function saveAsImage() {
    var imgData, imgNode;
    var strDownloadMime = "image/octet-stream";
    try {
        var strMime = "image/jpeg";
        var canvas = document.querySelector('#myEmbeddedScene canvas');
        imgData = renderer.domElement.toDataURL(strMime);
        //saveFile(imgData.replace(strMime, strDownloadMime), "test.jpg");
    } catch (e) {
        console.log(e);
        return;
    }
    var path = 'immagine';
    //loadImage(imgData,path);
    var cameraPos = [];
    for (var i = 0; i < prototype.binaries.length; i++) {
        camera.position.x = prototype.binaries[i].grids[1].mesh.position.x + camera_position[0];
        camera.position.z = prototype.binaries[i].grids[1].mesh.position.z + camera_position[2];
        camera.lookAt( new THREE.Vector3(-prototype.binaries[i].center_binary[0],0,prototype.binaries[i].center_binary[1]));

        cameraPos[0] = camera.position.x;
        cameraPos[1] = camera.position.y;
        cameraPos[2] = camera.position.z;

        renderer.setClearColor(0xffffff, 1);
        gridHelper.visible = false;

        var grids_name = []
        grids_name.push(prototype.binaries[i].grids[0].name);
        grids_name.push(prototype.binaries[i].grids[1].name);
        grids_name.push(prototype.binaries[i].grids[2].name);

        // TODO array stringhe da visualizzare nella prima immagine, e nella seconda else false


        var screenNameVisible = [];
        screenNameVisible.push(prototype.binaries[i].supportAList[0].name);
        screenNameVisible.push(prototype.binaries[i].rail.name);
        screenNameVisible.push(prototype.binaries[i].sleeper.name);

        makescreenshoot(i , screenNameVisible, prototype.binaries[i].supportAList[0].name, "supportA");

        for(var j = 0; j< prototype.binaries[i].rail.mesh.length; j++){
            prototype.binaries[i].rail.mesh[j].material[1].opacity = 0.3;
        }


        screenNameVisible = [];
        screenNameVisible.push(prototype.binaries[i].supportBList[0].name);
        screenNameVisible.push(prototype.binaries[i].supportAList[0].name);
        screenNameVisible.push(prototype.binaries[i].rail.name);
        screenNameVisible.push(prototype.binaries[i].sleeper.name);

        makescreenshoot(i , screenNameVisible, prototype.binaries[i].supportBList[0].name, "supportB");


        screenNameVisible = [];
        screenNameVisible.push(prototype.binaries[i].supportLList[0].name);
        screenNameVisible.push(prototype.binaries[i].supportAList[0].name);
        screenNameVisible.push(prototype.binaries[i].rail.name);
        screenNameVisible.push(prototype.binaries[i].sleeper.name);

        makescreenshoot(i, screenNameVisible, prototype.binaries[i].supportLList[0].name, "supportL");

        for(var j = 0; j< prototype.binaries[i].rail.mesh.length; j++){
            prototype.binaries[i].rail.mesh[j].material[1].opacity = 0.8;
        }


        screenNameVisible = [];
        screenNameVisible.push(grids_name[0]);
        screenNameVisible.push(grids_name[1]);
        screenNameVisible.push(grids_name[2]);
        screenNameVisible.push(prototype.binaries[i].rail.name);
        screenNameVisible.push(prototype.binaries[i].sleeper.name);

        makescreenshoot(i , screenNameVisible, grids_name[1], "grids");

        renderer.setClearColor(0xdddddd, 1);
        gridHelper.visible = true;
        reorderAll();
    }

}
function makescreenshoot(binary, secondScreen, support_name, nameofType) {

    scene.traverse(function (object) {
        if (object.name.indexOf(STARTNAMEOFALLMESH) != -1) {
            if (secondScreen.indexOf(object.name) != -1) {
                object.visible = true;
            } else {
                object.visible = false;
            }
        }
    });
    renderer.render(scene, camera);
    loadImage(binary, renderer.domElement.toDataURL("image/jpeg"), support_name.concat("_2"), prototype.id, nameofType);
}
function reorderAll() {
    scene.traverse(function (object) {
        if (object.name.indexOf(STARTNAMEOFALLMESH) != -1) {
               object.visible = true;
        }
    });
    camera.position.x = camera_position[0];
    camera.position.y = camera_position[1];
    camera.position.z = camera_position[2];

}
function loadImage(binary, imgData, path, id_prototype, type) {
    $.ajax({
        url: "/uploadImage",
        data: {binary: binary, img_data: imgData, name_file: path, id_prototype: id_prototype, type: type},
        type: "POST",
        contentType: "application/x-www-form-urlencoded"
    }).done(function (data) {
        console.log(data);
    });
}


/**var saveFile = function (strData, filename) {
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
        document.body.appendChild(link); //Firefox requires the link to be in the body
        link.download = filename;
        link.href = strData;
        link.click();
        document.body.removeChild(link); //remove the link when done
    } else {
        location.replace(uri);
    }
}
 **/
function setVisible(span, id) {
    var changed = false;
    scene.traverse(function (object) {
        if (object.name === id) {
            object.visible = !object.visible;
            if (!object.visible && !changed) {
                $(span).removeClass('glyphicon-eye-open');
                $(span).addClass('glyphicon-eye-close');
                changed = true;
            } else if (object.visible && !changed) {
                $(span).addClass('glyphicon-eye-open');
                $(span).removeClass('glyphicon-eye-close');
                changed = true;
            }
        }
    });
}
function setVisibleGrid(span, binaryname) {
    var changed = false;
    scene.traverse(function (object) {
        if(object.name.indexOf(binaryname+"_grid") != -1) {
            object.visible = !object.visible;
            if (!object.visible && !changed) {
                $(span).removeClass('glyphicon-eye-open');
                $(span).addClass('glyphicon-eye-close');
                changed = true;
            } else if (object.visible && !changed) {
                $(span).addClass('glyphicon-eye-open');
                $(span).removeClass('glyphicon-eye-close');
                changed = true;
            }
        }
    });
}
function moveCamera(span, id) {

    for (var i = 0; i < prototype.binaries.length; i++) {
        if (prototype.binaries[i].camera === id) {
            camera.position.x = prototype.binaries[i].grids[1].mesh.position.x + camera_position[0];
            camera.position.z = prototype.binaries[i].grids[1].mesh.position.z + camera_position[2];
            camera.position.y = camera_position[1];
            camera.lookAt( new THREE.Vector3(-prototype.binaries[i].center_binary[0],0,prototype.binaries[i].center_binary[1]));
            binaryWiewer =i;
        }
    }
}
function plus(span, id) {
    var changed = true;
    scene.traverse(function (object) {
        if (object.name === id && changed) {
            OBJECT_LOADED.push(object.clone());
            changed = false;
        }
    });
}
